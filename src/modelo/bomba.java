package modelo;

public class bomba {
    private int numbomba;
    private gasolina gas; //Gasolina es la otra clase de la que agrega
    private float capacidad,acumulador;
    
    public bomba(){
        this.numbomba=0;
        this.gas=null;
        this.acumulador=0;
        this.capacidad=0;
    }

    public bomba(int numbomba, gasolina gas, float capacidad, float acumulador) {
        this.numbomba = numbomba;
        this.gas = gas;
        this.capacidad = capacidad;
        this.acumulador = acumulador;
    }
    public bomba(bomba obj){
        this.numbomba = obj.numbomba;
        this.gas = obj.gas;
        this.capacidad = obj.capacidad;
        this.acumulador = obj.acumulador;  
    }

    public int getNumbomba() {
        return numbomba;
    }

    public void setNumbomba(int numbomba) {
        this.numbomba = numbomba;
    }

    public gasolina getGas() {
        return gas;
    }

    public void setGas(gasolina gas) {
        this.gas = gas;
    }

    public float getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(float capacidad) {
        this.capacidad = capacidad;
    }

    public float getAcumulador() {
        return acumulador;
    }

    public void setAcumulador(float acumulador) {
        this.acumulador = acumulador;
    }
    
    public void iniciarbomba(int numbomba,gasolina gas,float capacidad,float acumulador){
        this.numbomba = numbomba;
        this.gas = gas;
        this.capacidad = capacidad;
        this.acumulador = acumulador;
    }
    
    public float inventarioGasolina(){
        return this.capacidad - this.acumulador;
    }
    public float venderGasolina(float cantidad){
        if(this.inventarioGasolina() < cantidad){
          return 0.0f;  
        }
        this.acumulador+=cantidad;
        return cantidad * this.gas.getPrecio();
        }
    public float ventasTotales(){
        return acumulador * this.gas.getPrecio();
    }
    }