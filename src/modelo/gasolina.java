package modelo;

public class gasolina {
    private int idgasolina;
    private String tipo;
    private float precio;
    
    public gasolina(){
        this.idgasolina=0;
        this.tipo="";
        this.precio=0.0f;
    }

    public gasolina(int idgasolina, String tipo, float precio) {
        this.idgasolina = idgasolina;
        this.tipo = tipo;
        this.precio = precio;
    }
    public gasolina(gasolina obj){
        this.idgasolina = obj.idgasolina;
        this.tipo = obj.tipo;
        this.precio = obj.precio;
    }

    
    
    public int getIdgasolina() {
        return idgasolina;
    }

    public void setIdgasolina(int idgasolina) {
        this.idgasolina = idgasolina;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }
    
}