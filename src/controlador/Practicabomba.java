package controlador;

import modelo.gasolina;
import modelo.bomba;
import vista.dlgBomba;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;



public class Practicabomba implements ActionListener {
    private bomba bom;
    private dlgBomba vista;
    private int contador;
    
    public Practicabomba(bomba bom, dlgBomba vista){
        this.bom=bom;
        this.vista=vista;
        this.vista.btniniciarBomba.addActionListener(this);
        this.vista.btnRegistrar.addActionListener(this);
    }
        
    public void iniciarVista(){
        this.vista.setTitle("bomba");
        this.vista.setSize(648,337);
        this.vista.setVisible(true);
    }        
    
    public static void main(String[] args) {
        bomba obj=new bomba();
        dlgBomba vista=new dlgBomba(/*new JFrame(),true*/);
        Practicabomba controlador=new Practicabomba(obj,vista);
        controlador.iniciarVista();
    }

    
    private boolean isVacio(){
        return this.vista.txtnumBomba.getText().equals("") || 
               this.vista.txtPrecio.getText().equals("");
    }
    
    private int isValidoIniciarBomba(){
        try{
            if(Float.parseFloat(this.vista.txtnumBomba.getText())<=0 ||
               Float.parseFloat(this.vista.txtPrecio.getText()) <=0){
                return 1;
            }
        }
        catch(NumberFormatException ex){
            
        }
        return 0;
    }
    
    
    private int isValidoRegistrar(){
        try{
            if(Float.parseFloat(this.vista.txtCantidad.getText()) <=0){
                return 1;
            }
        }
        catch(NumberFormatException ex){
        }
        return 0;
    }
    

    @Override
    public void actionPerformed(ActionEvent e) {
        
        if(e.getSource()==this.vista.btniniciarBomba){
            if(this.isVacio()){
                JOptionPane.showMessageDialog(this.vista,"Tienes campos vacios, vuelve a capturar","bomba",JOptionPane.WARNING_MESSAGE);
                return;
            }
            if(this.isValidoIniciarBomba()==1){
                JOptionPane.showMessageDialog(vista, "revisa que no tengas numeros negativos","bomba",
                JOptionPane.WARNING_MESSAGE);
                return ;
            }
            else if(this.isValidoIniciarBomba()==2){
                JOptionPane.showMessageDialog(vista, "Tienes letras donde deben de ir numeros",
                "bomba",JOptionPane.ERROR_MESSAGE);
                return;
            }
            
            this.bom.iniciarbomba(Integer.parseInt(this.vista.txtnumBomba.getText()),
                    
                    new gasolina(this.vista.cmbtipo.getSelectedIndex() + 1,
                    this.vista.cmbtipo.getSelectedItem().toString(),
                    Float.parseFloat(this.vista.txtPrecio.getText())),
                    
                    this.vista.jSCantidad.getValue(),
                    
                    0.0f);
           
            this.vista.btniniciarBomba.setEnabled(false);
            this.vista.txtnumBomba.setEnabled(false);
            this.vista.txtContador.setEnabled(false);
            this.vista.txtPrecio.setEnabled(false);
            this.vista.jSCantidad.setEnabled(false);
            this.vista.cmbtipo.setEnabled(false);
            this.vista.txtCantidad.setEnabled(true);
            this.vista.btnRegistrar.setEnabled(true);
        }
        
        
        if(e.getSource() == this.vista.btnRegistrar){
            if(this.vista.txtCantidad.getText().equals("")){
                JOptionPane.showMessageDialog(this.vista,"Tienes campos vacios, vuelve a capturar","bomba",JOptionPane.WARNING_MESSAGE);
                return;
            }
            
            if(this.isValidoRegistrar()==1){
                JOptionPane.showMessageDialog(vista, "revisa que no tengas numeros negativos","bomba",
                JOptionPane.WARNING_MESSAGE);
                return;
            }
            else if(this.isValidoRegistrar()==2){
                JOptionPane.showMessageDialog(vista, "Tienes letras donde deben de ir numeros",
                "bomba",JOptionPane.ERROR_MESSAGE);
                return;
            }
            
            float venta= this.bom.venderGasolina(Float.parseFloat(this.vista.txtCantidad.getText()));
            if(venta != 0.0f){
            this.vista.lblCosto.setText(String.valueOf((venta)));
            this.vista.lblTotalVenta.setText(String.valueOf(this.bom.ventasTotales()));
            this.contador++;
            this.vista.txtContador.setText(String.valueOf(this.contador));
            this.vista.jSCantidad.setValue((int)this.bom.inventarioGasolina());
            }
        }
    }
}


















/*package controlador;

public class Practicabomba {

    public static void main(String[] args) {
        // TODO code application logic here
    }
}
* */
